#!/bin/bash
set -e

ACTION="backup"
S3_CONFIG_DIR=${S3_CONFIG_DIR-/etc/gitlab}

if [ ! -f $S3_CONFIG_DIR/accesskey ]; then
  echo "Can not find s3 config file at $S3_CONFIG_DIR/accesskey or $S3_CONFIG_DIR/secretkey"
fi

export S3_ACCESS_KEY=$(cat $S3_CONFIG_DIR/accesskey)
export S3_SECRET_KEY=$(cat $S3_CONFIG_DIR/secretkey)
export BACKUP_BUCKET_NAME=${BACKUP_BUCKET_NAME-gitlab-backups}

rails_dir=/home/git/gitlab
backups_path=$rails_dir/tmp/backups
backup_tars_path=$rails_dir/tmp/backup_tars

function usage()
{
  cat << HEREDOC

   Usage: backup-utility [--restore] [-f URL]

   optional arguments:
     -h, --help                             show this help message and exit
     --restore [-t TIMESTAMP | -f URL]      when specified utility restores from an existing backup specified
                                            as a url or a timestamp of an existing backup in object storage
HEREDOC
}

# Checks if provided argument is a url for downloading it
function is_url() {
  regex='(https?|ftp|file)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'

  [[ $1 =~ $regex ]]
}

function fetch_remote_backup(){
  mkdir -p $backups_path
  output_path=$backups_path/0_gitlab_backup.tar

  if is_url $1; then
    >&2 echo "Downloading from $1";
    curl --progress-bar -o $output_path $1
  else # It's a timestamp
    file_name="$1_gitlab_backup.tar"
    s3cmd sync "s3://$BACKUP_BUCKET_NAME/$file_name" $output_path > /dev/null
  fi

  echo $output_path
}

function unpack_backup(){
  local file_path=$1
  cd $(dirname $file_path)

  echo "Unpacking backup"

  if [ ! -f $file_path ]; then
    echo $file_path not found
    exit 1
  fi

  tar -xf $file_path
}

function pack_backup(){
  echo "Packing up backup tar"
  local backup_name=$1
  tar -cf ${backup_tars_path}/${backup_name}.tar -C $backups_path .
}

function get_version(){
  cat $rails_dir/VERSION
}

function get_backup_name(){
  if [ -n "$BACKUP_TIMESTAMP" ]; then
    echo ${BACKUP_TIMESTAMP}_gitlab_backup
  else
    now_timestamp=$(date +%s_%Y_%m_%d)
    gitlab_version=$(get_version)
    echo ${now_timestamp}_${gitlab_version}_gitlab_backup
  fi
}

function cleanup(){
  rm -rf $backups_path/*
}

function write_backup_info(){
  cat << EOF > $backups_path/backup_information.yml
:db_version: $($rails_dir/bin/rails runner "File.write('/tmp/db_version', ActiveRecord::Migrator.current_version.to_s)" && cat /tmp/db_version)
:backup_created_at: $(date "+%Y-%m-%d %H:%M:%S %z")
:gitlab_version: $(get_version)
:tar_version: $(tar --version | head -n 1)
:skipped: $1
EOF
}

function get_skipped(){
  all=( artifacts.tar.gz uploads.tar.gz builds.tar.gz db lfs.tar.gz registry.tar.gz pages.tar.gz )
  skipped_string=""

  for backup_item in ${all[@]}; do
    if [ ! -e $backups_path/$backup_item ]; then
      skipped_string="$skipped_string,${backup_item%.tar.gz}";
    fi;
  done;

  echo ${skipped_string#,}
}

function backup(){
  backup_name=$(get_backup_name)
  mkdir -p $backup_tars_path

  gitlab-rake gitlab:backup:db:create
  gitlab-rake gitlab:backup:repo:create
  object_storage_backed=(registry uploads artifacts lfs)

  for backup_item in ${object_storage_backed[@]}; do
    object-storage-backup $backup_item $backups_path/${backup_item}.tar.gz
  done

  skipped=$(get_skipped $backup_name)
  write_backup_info $skipped
  pack_backup $backup_name
  s3cmd sync ${backup_tars_path}/${backup_name}.tar s3://$BACKUP_BUCKET_NAME > /dev/null

  echo "[DONE] Backup can be found at s3://$BACKUP_BUCKET_NAME/${backup_name}.tar"
  cleanup
}

function is_skipped() {
  [[ $SKIPPED =~ $1 ]]
}

function restore(){
  if [ -z "$BACKUP_URL" ] && [ -z "$BACKUP_TIMESTAMP" ]; then
    echo "You need to set BACKUP_URL or BACKUP_TIMESTAMP variable"
    exit 1
  fi

  BACKUP=${BACKUP_URL-}
  if [ -z "$BACKUP" ]; then
    BACKUP=$BACKUP_TIMESTAMP
  fi

  file=$(fetch_remote_backup $BACKUP)

  dir_name=$(dirname $file)
  file_name=$(basename $file)
  timestamp="${file_name%%_*}"
  export BACKUP=$timestamp
  unpack_backup $file

  skipped_line=$(cat $(dirname $file)/backup_information.yml | grep skipped)
  export SKIPPED=$(echo ${skipped_line#:skipped:})

  ! is_skipped "db"           && gitlab-rake gitlab:db:drop_tables
  ! is_skipped "db"           && gitlab-rake gitlab:backup:db:restore
  ! is_skipped "repositories" && gitlab-rake gitlab:backup:repo:restore
  ! is_skipped "builds"       && gitlab-rake gitlab:backup:builds:restore

  object_storage_backed=(registry uploads artifacts lfs)
  for restore_item in ${object_storage_backed[@]}; do
    if [ -f $backups_path/${restore_item}.tar.gz ]; then
      ! is_skipped $restore_item && object-storage-restore $restore_item $backups_path/${restore_item}.tar.gz
    fi
  done

  gitlab-rake cache:clear
}

while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    -h|--help)
      usage
      ACTION="none"
      break
      ;;
    -f|--file)
      BACKUP_URL="$2"
      shift
      shift
      ;;
    -t|--timestamp)
      BACKUP_TIMESTAMP="$2"
      shift
      shift
      ;;
    --restore)
      ACTION="restore"
      shift
      ;;
    *)
      shift
      ;;
  esac
done

if [ "$ACTION" = "restore" ]; then
  restore
elif [ "$ACTION" = "backup" ]; then
  backup
fi
